# Very Cool People

## INTRODUCTION - SKIP THIS IF YOU JUST WANNA GET TO THE GOOD STUFF

“I want to do something about all this capitalism business, but nobody will tell me what to do except ‘the work.’”

Don’t worry fam we got you covered! We were kind of sick of people just saying “do the work” without ever getting down to the low-level implementation of what the work is besides “join your local group/organize,” and we realized that the reason for this is that nobody actually knows what that means. And we’re trying to do something that, frankly, is beyond common sense - we’re trying to imagine a world beyond capitalism, a fairer one for all peoples, and then we’re trying to figure out how to bring that world into being. That’s a big order. That’s scary. But despite us having been in “late-stage” capitalism for several decades now and people telling us “the revolution” is happening any day now, we continue chugging on into the future, just as capitalist as ever. If you’re waiting for Red October to happen so you can join in, you’re gonna be waiting a while. Looks like we need to try something else.

So what can we do? Honestly, there is so much to be done that everybody can probably contribute in a different field in their own way. You don’t  need to do every single item on this list. You don’t need to follow whatever the latest fashion is. What you need is to understand your goal and how you can achieve that goal. We are trying to look in the short term (things you can start on today), the medium term (five years), and the long term (ten+ years), because we need people on different scales of action and we need that for every aspect of society. Remember, we are re-organizing society. Society is big. Society means agriculture, mining operations, processsing raw materials into things, logistics, supply chains, emotional labor, medicine, labor, organizing labor (not in the union way but in the ‘let’s all work together in an effective way’  way), construction, housing, distribution of resources, etc. 

I am guessing that you are not all experts in all of these things. That’s fine - you don’t have to be. You can find one thing you are passionate about and start learning about that thing while trying to apply a leftist lens to it. If you are interesting in supply chain management (that’s going to be very important in the future, since “workers owning the means of production” will change a lot of how the supply chain works and what things cost), try to think about how you’re being taught about things, the environmental impact of your actions (try to consult your environmentally inclined friends), the social impact (your gender studies friends etc.), and what a supply chain may look like in a leftist and ecologically friendly world. Would we have to stop international shipping? Or is there a way to do so, but on a reduced level? These are the questions you need to investigate first, so that you can then look at multiple solutions. Once you have envisioned your solution, there comes the problem of implementing it. Supply chains are enormous so it’s not like one person can make a change, but start imagining a different type of supply chain. Keep up with new developments. Talk to the people on the ground whenever possible and run your ideas past them, and listen very intently to them.

A lot of the following ideas come from the “Solarpunk” movement. Solarpunk is vaguely defined as an optimistic future that is environmentally sustainable and environmentally integrated. The solarpunks I’ve taken this from are mostly anarchists or anarcho-communists, meaning that they want to envision a world free of a state and where workers own the means of production. A lot of the actions below are based around freeing yourself from capitalist control as much as humanly possible and creating communities that model anarchist/solarpunk values. 

The first value is trying to avoid feeding into capitalism where possible. We are trying to, in a sense, stop or change capitalism, so changing our consumption and participation will be important. “But there’s no ethical consumption under capitalism anyway!” Sure, that’s true. But does that mean that all consumption is equally unethical? I feel like there’s a world of difference between buying cotton from a Bangladeshi farmer who was paid a living wage for their work and had proper safety precautions and buying cotton from a Bangladeshi farmer who was blinded from spraying chemicals on the crops. They are both exploitative still, but one of these will result in someone being able to feed their family and circulate money in their economy, while the other one results in poverty and leaves someone blind for cheaper cotton. “Harm reduction” may be often-mocked, but it is still important. (NOTE: if you are questioning whether the prior option of a well-paid, safe cotton farmer exists, nice observation! In many places, a “harm reduction” option doesn’t even exist! When that happens, we need to either get creative or take the L and accept that we’re not going to beat capitalism today.) This also isn’t just about ~*~consuming ethically~*~ - you cannot shop your way into anarchy. Part of this is also avoiding giving capitalists your data and information, which they can then use against you. Finally, this can also involve making and repairing things yourself, in a way attempting to exit consumerism

The second value is modeling anarchist values. This is stuff like mutual aid, forming a community, trying to help people. This can be harm reduction or it can be more radical - it can be an example of how to live. A lot of people say something like “we cannot have an anarchist society - it has never been done in our present time.” I know you will be tempted to say “but Rojava!” and I am not telling you not to say that. I am saying that it will be much more impactful if you can tell someone “but it’s already happening right here in your community!” If you can successfully model a miniature anarchist society, to the extent that it is possible, you break down a barrier. It stops being “this is impossible” and starts being about “how possible/desirable is this on a larger scale?” And you have successfully changed the topic. Some of you readers will say, “there is no point in doing this because capitalism crushes all attempts at escaping the system.” And it is true, capitalism does do that one way or another. But in a way, we need to show that failure to people. People think anarchism fails because of something inherent to anarchism. If we show them that anarchist societies are ultimately snuffed out by capitalism - and they see this happening in front of them - then their ire becomes directed at capitalism for snuffing out the incredible thing happening in front of them. In a way, the sacrifice is the point. Not to be cheesy, but this was part of the idea behind Martin Luther King Jr.’s civil disobedience. The point was to bear the violence of the state to show the world - “this is what happens when black folks peacefully take what’s theirs in society.” And although he wasn’t a popular figure back then, the sacrifice that he and his compatriots made utterly changed the conversation about race. We need to show both that anarchist societies are possible and that stopping capitalism is a necessary part of making those societies possible on a large scale. Praxis beats out theory memory-wise.

“But I am too poor/disabled/something else to do this stuff!” We have trust that you are capable of analyzing your own capacities and determining yourself what you can and cannot handle. The intention is not to force a socially anxious person into knocking on doors; it is to match people up with the actions they can take that match their talent, temperament, and living situation. We ask that you be honest with yourself. At the end of the day, we cannot put you in anarchist jail for being a bad comrade. You must answer to your own conscience.

--found on tumblr from socalledunited:

Join a local union and/or the IWW. 

Join or start a local tenant’s union. 

Volunteer with Food Not Bombs and do other mutual aid in your community. 

Support your local solidarity economy and maker community. 

Build, fix, and grow stuff. 

Use free, open-source software and stop letting companies sell your data. 

Don’t let companies get your money if they don’t deserve it, especially with regards to media.

Civil disobedience. 

Attend local actions


And most importantly, join radical groups in your area. Strength is found in numbers and none of us can change the world alone.

First, I super recommend that everybody listen to this:  https://rebelsteps.com/about/  It’s a podcast with short episodes in plain language that do a lot to answer these very questions.


If you or your pals stay in apartments or know folks that do, you could start a tenants’ association

You could start a Food Not Bombs / STP group and give food and supplies to vulnerable people

Help people suffering from drug addiction by starting a harm-reduction group

And if you want a downright exhaustive list of ideas and tips (even if the politics are a bit dodgy), The Secret Is To Begin

## MUTUAL AID:

Many people are demotivated from providing mutual aid to their community because they assume it’ll be expensive, complicated, and/or illegal. Not so! You can make a big difference in your neighbors’ lives just by sharing what you have and building solidarity with them (Note: Your local public library, community garden, or makerspace may be happy to host one or more of these if you get in touch with them!)
Connect on social media: A good first step is to start a group message, Facebook group, Discord server, email list, or anything else that allows people to talk to each other consistently. From here you can start to build solidarity and discuss what unmet needs your community has and how to address them

Bartering, time banking, and free stores: From there it’s easy enough to start the conversation about how to provide each other with material aid. Poor communities often have needs and the ability to meet those needs right next to each other, but they are never matched together just because people don’t have enough pieces of paper to give each other. A way around that is to start trading with each other - a jar of jam for an old unused bike, car repairs for fresh honey, 2 hours of guitar lessons for 2 hours helping repaint your deck, etc. This doesn’t have to be formalized and kept precise track of - “I’ll owe you one”s or just a general culture of “homies help homies, always” are a great way to build up trust and a sense of community

Buying coop: Buying wholesale can save a lot of money, but a lot of people never have enough money at one time to take advantage of that (besides not needing an entire pallet of dish soap). A buying coop lets a group of people pool their money to buy wholesale instead, saving everyone money in the process

Food sharing: Food waste/excess and food insecurity are a perfect match, and as such there have been plenty of ideas for bringing them together. Community pantries can pool and hold nonperishables inside something like an apartment building; a simple veggie share can be built and set up outside for communities with a lot of gardeners/farmers; a people’s fridge can hold perishable items if you can get a hold of the appliance itself and space/power for it; a setup like MIT’s FoodCam can connect hungry people to unwanted leftovers; and Food Not Bombs collects commercial food waste to share with communities all across the world (your city might very well have one already - look it up!)

Tool share/library of things: There are a lot of things that you use once or twice a year at most and then let sit in your attic the rest of the time. Post hole diggers, Batman-shaped cake molds, 3D printers, turkey basters, etc. Not only do these sit around unused for a majority of the time, but worse, if someone in your community needs one, they’ll likely go out and buy a whole new one. Instead, try finding a community area where you can put a lending library. That way, everyone in your community can pool their scarcely-used resources, and rather than owning 20 hammers altogether, everyone can share 2

Little free seed library: Little free libraries are a fairly well-known way to informally share resources with your neighbors, but if you live in an area with a lot of gardeners, or hopeful gardeners, saving seeds can be another way to use the same design. Just set aside the seeds from your harvest, or your groceries, then put them in reused envelopes from your junk mail and leave them in the box to be picked up by your neighbors. You can also share cuttings and clones from your garden!

Community composting: Composting is a dead simple process for turning food scraps and other organic waste into rich fertilizer - saving any gardeners in your neighborhood money (and keeping harmful pollutants out of the environment) with no ongoing investment needed. Personal setups require specific inputs in specific quantities for best results, but in large enough piles those rules can be largely ignored and still give great compost. If you get enough people involved, then, your composter can be as simple as a large box on the side of the road. ShareWaste can connect you to people with food scraps, whether you have a personal or community composter

Bike/car sharing: People aren’t moving around at all hours, meaning a majority of the time their transportation is sitting in garages or driveways unused. Starting a bike share can be a fairly easy, low-cost way to address this and improve people’s mobility. A car share can be a lot more involved, but worth it for all the money and carbon it can save (especially for areas with little or no public transit)

## AGRICULTURE:


Growing your own food is a great way to become more independent from capitalist infrastructure, but if you’re spending hundreds of dollars just to get started it can almost feel counterproductive. There are plenty of great tutorials for making your own pots, garden boxes, rainwater collectors, composters, etc. from reclaimed materials, which can bring the cost down significantly, but the cost of the plants alone can really start to add up if bought from a nursery or garden store. 

So I’ve compiled a few ideas for getting your garden started for free. If you know of any other tricks, please feel free to add them in reblogs!

FROM FOOD

Set aside the seeds from your produce bought from the grocery store. Just make sure the brand you’re buying from doesn’t contain terminator genes first. Organic or non-GMO is preferred.

Almost all dry beans can simply be planted in the ground and will most often sprout. They’re easy to grow in warmer climates and very hardy.

You can grow many potato plants from a single potato with eyes. Cut the potato into chunks with each chunk having an eye sprout, and place the chunks in the ground.

Numerous varieties of onions, brassica oleracea vegetables, and celery can be regrown from the base, after re-sprouting the plant in a dish of water for days.

Many root vegetables can be regrown from their leafy tops (which are also edible and tasty in salad). Carrots, radishes, beets, rutabagas, and more can be regrown from tops.


Pineapples can be regrown from their tops, if you live in the right climate or have a greenhouse/cold frame.

If you can, shop at your local farmer’s market and support local farmers, instead of big-name grocery stores!

FROM THE WILD

Look up edible plants that grow wild in your area and how to replant them. You can take seeds from any plant, obviously, but some can also be regrown from cuttings, suckers, scraps, etc.

Transplanting is also an option, but not preferable since it removes a plant from the environment.

Native plants are guaranteed to be a good fit for your environment, so this is a great lesser-care option.

FROM NEIGHBORS

Of course, the same methods that work for food/wild plants (planting seeds, growing from scraps or cuttings, etc) will also work for food and plants from friends’ gardens. If you know anyone already into gardening, ask them to help you get started! If you see someone gardening in your neighborhood, say hi!

Many fruit trees can be cloned from cuttings. Cut off a branch at a 45-degree angle, then soak the cut end in water for ~5 minutes before applying rooting hormone and placing it into potting soil. Caring for the clone and other specifics can vary by species, so do a quick search beforehand.

Grafting fruit trees is a millenia-old technique, wherein a cutting from a fruit tree is inserted into a hardier rootstock tree and bonded with a special wax. The tissues intermingle to form a single tree. Look online for tutorials, and be sure to take the proper precautions.

Organize a seed swap or seed bank in your area.

Found or join a chapter of Food Not Lawns, or a similar organization.


## IMPORTANT EXERCISES:

How are capitalists holding power? Which of these methods of power/means of productions are old and which ones are new? Which ones are located in our country? (I am sorry to say that if your dream was to participate in an uprising where you take over a factory, there are not too many factories left if you are US-based.)

Although theory is important for creating a morally consistent world, we will also need people who have practical every day skills for building a society. Some examples of this are:

Growing food

Cooking food

Storing food

Textile production

Sewing

Electronics repairs

HVAC

How to run a house (how to clean, hygiene)

Engineering

Physical labor

Carpentry

Self-defense

Basically, if someone in a zombie apocalypse group thinks it would be a useful skill. You don’t need to learn everything, but try to have one skill. For example, I’m trying to learn about sewing and textiles. Someone else near me may prefer carpentry. Having a community with a diverse skillset is important. Also, feel free to teach each other skills. Knowing the basics can often take you pretty far.

There are also skills that may be less practical in an apocalypse situation that we will still need people to learn, such as:

Programming

Management

Electronic repair

Social workers 

Liberians/Archivists

Event Management

Idk add your own here

Environmentalstuff

Obviously being a programmer won’t save you in the post-apocalyptic world, but if we still have energy it can be pretty useful.

Basically, don’t limit yourself to just theoretical stuff. I know it’s fun to pontificate about the true nature of the divine masculine, but we also need to like… clean houses and stuff. It’s not always going to be glamorous. But it’s necessary in every human society. At least until we have the psot work FULLY AUTOMATED LUXURY GAY SPACE COMMUNISM. But until that day, someone’s gotta sweep the floors and grow the food.

## Talk to people!

Oh no! But I’m an introvert! Hey, talk to your friends! And even family! You need to practice talking to people about leftism/solarpunk/anarchy/whatever it is you wanna do. That way you know what arguments are effective and which ones are not. You also learn what people’s concerns are and what their values are. Dont’ fall into the trap of thinking “ugh, they’re a LiBeRaL/CoNsErVaTiVe! They’ll never get it!” Listen, I have the receipts that you were an anarcho-capitalist in 2007 and said you didn’t get gay people in 2008. People can learn and people can change. Unless you were raised by the perfect leftist parents and had the perfect leftist peers, you probably had some stupid political opinions too. But you changed, didn’t you? Because someone took a chance on you and actually explaining something to you instead of saying “educate yourself, edgelord”. Because some people took time out of their lives to write about the issue in detail on the internet so people like you could learn. And now you’re a gay  anarcho-communist. Nobody is going to change their entire political opinion in a single day. But over time, you can start getting them to see it yoru way. Before you know it, you may have a new comrade.

And even if they’re stalwart liberals or conservatives, here’s the thing… they can still help you in your mission. Just like well meaning people donate to the Salvation Army and accidentally help an anti-LGBT organization, you can get well meaning libs and cons to help you with a community garden or starting up a recycling operation or any number of things. They probably won’t join you on the more radical things, but we need people to occupy every point of radicalness to pull this off. The most cynical interpretation of this is you can always use another warm body. The optimist interpretation is that people may not yet know how they’re ready to act. Okay, that’s patronizing, but I’m just saying don’t cut people off if you don’t have to. (Note: I trust you know what “have to” means. You don’t have to talk to racist Randy at Thanksgiving if you’re black. But if you’re white… give it a try and talk to him when you’re totally alone.)

## Join local political organizations!

“Oh no, are you telling me to vote? Nice try, lib, we all know voting is just the way that the liberal capitalist ELITES let us pretend we have power.” Okay, you get me, I have indeed been pwned. But voting is a form of harm reduction. And if voting is so pathetic and unnecessary, why do shadowy figures like the Koch brothers spend billions trying to stop people from voting? If they really do the same thing no matter what, they shouldn’t care who’s voting if it supposedly leads to the same conclusion every time. Nobody’s pumping money into North Korean elections trying to sway the result; it always comes back to Kim Jong Un anyway. The reason they gerrymander and suppress the vote is because they know we can still express power through that, and they cannot let us have even that. True, voting is limited. But there’s a reason they want us to not even have that sliver. That sliver could really inconvenience some capitalists.

If you have a leftist political party, join that, but if you are stuck between “liberals” and “conservtaives,” you can sneak into the liberal party. Heck, look at me. I’m in my local political party. Guess what - it turns out we have an active progressive wing. THey don’t have enough people  yet to really move the needle, because it turns out very few youn people join the local party. I am technically an elected official of the party. I can even run for positions within the party. If you do this, you can actually wield local political power. 

Now why haven’t I done this? BEcause I would be a shit politician, my friend. Not everyone is cut out to be a politician. But maybe you’re thinking, I could do that. And if you can, I will support you. You can have people support you. Bring YOUR people to the party. Like I said , we are waging a war on all  fronts. We need our people everywhere. That’s leftist parties and liberal parties. Hell, do it in a consertvative party if you have the strongest, steeliest guts of any human being out there. If you’re in a party long enough, you can basically get elected to really low level stuff and start climbing through there.

Organize the vote. Meetup with all sorts of different groups,not just libs.  Please don’t just… not vote. There are many people who need the meagre protections of the law. I know that if you’re in the empire, your heart may burn for the people outside the empire who will be hurt by th eempire no matter what. But you still have political power. I don’t know how we can stop the US from being interventionist yet. But staying outside the system has yet to solve anything. Being inside the system buys time. And nobody is saying just vote. That’s the point of this entire freaking article. There’s so much you can do besides vote. There are different ways to organize and prepare. None of these take time away from voting.

## The Petty Bouge

I’m the petty bourgeouisie and a member of the managerial class, but screw capitalism. What can I do?

Buy houses for people? I don’t know

If your friends are striking, pay their wages?

Help an unemployed friend out?

Idk but we need to involve our bougie allies. Yes i know it sounds crazy, but there’s white people like John Brown who fought against their “race interests” and fucking died to free slaves, so I’m not surprised there are members of the petty bougie and even the bougie who say, fuck capitalism. And they ARE powerful allies. So if they wanna help, I sya let em.

## SELF CARE

“Bath bombs as praxis, I knew it… this is liberal propaganda.” I haven’t even mentioned bath bombs but go off. Self care, which I shall define here as “taking care of yourself,” is indeed important. For example, it is very hard to organize for housing rights if you forget to eat for so long that you just die. That’s obviously a ridiculous example I just made up, but I think it gets the point across.

Eating. Self care is often framed with regards to food as “eat delicious food you like,” but I also consider eating healthy food as a form of self care. Make sure that you are getting the calories you need each day. If you are afraid you may have disordered eating patterns, you should see a doctor if you can. If you have food to spare, invite friends over to eat together.

Exercise. Try to do the exercise that your health lets you do. It can be as simple as doing some stretches. You don’t need to go to the gym if you don’t want to. I like to take a jog or just a walk if a jog is too much for me. Exercise can be good for your brain as well as your body. 

Social. Stay in touch with friends for reasons other than organizing. If you just talk to friends about organizing, they’re more like co workers than friends, are they? Talk to each other about how you’re feeling emotionally. Go out together to do something silly. Call up an old friend. The journey of organizing for a new society can be hard and lonely, and capitalism is inherently alienating. It is critical to keep your bonds together. If you are on good terms with family, talk to them.

Spiritual. This sounds cheesy but it’s stuff like, how do you see yourself in the universe? If you feel meaningless or restless in the face of organizing and working, give yourself time to answer that question. If you are spiritual or religious, continue your faith practices. If you are a materialist, give yourself opportunities to heal yourself when you feel worn out. It is okay to rest, to have fun. Our revolution cannot run solely on anger. We also need to experience joy and wonder to remember that these, too, are things worth fighting for and things that everyone should have. You don’t need to be a martyr for the cause. You have a life and it is worth living too, not just spending every waking moment of it in guilt. If I wanted to live like that, I’d go back to Roman Catholicism! *rimshot*

Health. Mental health and physical health! See doctors! Don’t push yourself so hard that you hurt yourself. Know your limits and do not be afraid to enforce your boundaries. 


## Intro Organizing
 
You have probably heard someone say “you need to organize!” at some point and felt utterly annoyed by the lack of specificity of this phrase. What the heck is “organizing”? I remember following the 2008 election and hearing people talk about Obama’s experience as a “community organizer.” I had no idea what that meant. What, did he organize office parties? Get people together for surprise birthdays? Clean up litter? It was such a vague phrase and nobody ever explained it to me.
 
Thankfully I have ~done the work~ so that you don’t have to. As best as I see it, “organizing” really refers to “organizing groups of people to mobilize for a particular event.” That’s not much better, but let’s break it down.
 
Let’s say there’s some issue that you’re concerned about. For example, there’s a historically important building you want to prevent the city from tearing down to replace with another condo for rich foreigners to buy and never occupy.
 
Organizing here involves (a) spreading the word. Talk to people about why this thing is important. Try to convince them. Note – most people’s activism ends at this point. This is the beginning, not the end. If everyone knows why “tearing down the building is bad” and nobody does anything… then you’re not an activist, you’re just a Multi Level Marketer selling good feels instead of leggings. Don’t be the Ponzi Scheme of activism – commit to meeting up with people, at least.
 
(b) Once you have people who agree with you that This Thing Is Bad, get them on your side. For example, invite people over to your place or to a library or even *bougieness intensifies* a Starbucks. They have a third place policy now! /s The point is to go somewhere leisurely where you can talk safely for as long as you need. Brainstorm about ideas to accomplish your ultimate goal. For example, you may think that the problem is not enough people know about the building being torn down, and you may decide that you want to spread pamphlets (a little 18th century) or even… buy a Facebook ad. Or you may think that a protest in front of the building would be more effective. Whatever you do, try to come to an agreement about what to do and think about what your desired outcome is (e.g. 50 ppl come to our building protest) and the deadline (January 19th). Commit to this date. Make sure nothing else major is happening on this date!!
 
(c\) Seek out the wisdom of the elderly. Aka people who have done this before. Nothing wrong with being the new kids in town, but it’s worth it to talk to other people who have done something similar before. For example, in our case we may want to google “[location] historic preservation committees” or “[location] anti condo groups.” Keep a list of all the names you find. Then start contacting people. Try emailing them and explaining what you’re trying to do, and that you would appreciate pointers, or even to unify your two efforts for this particular purpose. Not everyone is going to agree, but cold calling is the same whether you’re a travelling salesperson or a community organizer.
 
(d) The day of the event, make sure everyone has a clear and defined role. E.g. jane is here to make sure no tanks or cars try to run us over. John hands out water bottles to the protestors. Ahmed makes sure the permits or whatever you had to acquire to protest are ready. Then… do the thing you want to do.
 
This is the roughest possible outline for what “organizing” means. The good news is you can start doing it yourself! Now your first attempts at organizing may not be super successful. My first attempt at organizing was a Facebook group where I invited all my liberal/left friends and posted articles about things we could do. I think half the people quit the group in the first week. It is okay to fail. Failure teaches you something. There is no way to guarantee success, no matter how urgent or important your cause is. Grieve if you must. It’s a tough world out there. But there are also victories to be won. When you’re ready, get back out there and win one for the gipper!
 
“But what if there are no groups near me?” Maybe you live in some kind of super rural area where there are 300 ppl and all of them are super conservative and also you have no car. Fair enough, organizing in an environment like this is more than an uphill battle. It’s like a 90 degree hill battle. Nevertheless, conservatives are still, you know, people with empathy and stuff. You will be surprised what conservatives will agree to when the cameras are off and they don’t have to perform conservatism for family and friends. If there are any people who are “okay”, it may be worth it to try to talk to them and slowly convince them. This will lay the groundwork for other activists in the future.
 
You can also try to do activism through the internet if you are certain that there is 0 hope for your town. Donating money to groups in your state or region that are fighting the good fight will make a big difference, more so than donating to a mega charity or something. You can do phone banking or text banking, or assist people in other places. There are not too many things you can do if there is absolutely nothing in your local area, unfortunately, but community organizing is just one type of activism.
 
## Liberal Democracy
 
If you live in a liberal democracy, you might be able to vote and participate in your political system! Although many leftists scoff at the idea of validating liberal democracy, the fact remains that if you have power, you should use it.
 
If you are located in the United States, Indivisible has an excellent guide on how to get involved in politics. You can get into your local party (try running for “precinct captain” – most precincts do not have a representative and if you’re lucky, you’ll just automatically be inducted into the party without having to compete in an election) and then gain a position of power in that party. Many Democrats have “progressive” wings which are ripe for moving left, if you know how to play it. If you can even gain local office (super-local representative, school board, etc.), that’s actual power that you have in your community. Obviously they will rake your past out if you try to run for something that gets a lot of media attention, so either sanitize your past to the best extent you can or just own it. This works better or worse depending on where you are. Nevertheless, don’t knock it till you try it.
 
How to Join Groups Using Google
 
Maybe none of this has been specific enough for you and you’re still lost. That’s where search engines come in. Maybe you’ve heard of this “Google” thing. Or if you hate Google and don’t want to give your information to Evil Empire #2, you want to DuckDuckGo it. (Gosh, that’s a terrible verb.) Here are some types of things you can Look Up to find groups in your area.
 
[blab la bla]
 
Email these groups. If they have a Facebook, swallow your pride, make a fake facebook with a name that kinda sounds like you, and message them. Call them if they have a phone number. Visit their headquarters, even. These people are probably busy, so you need to contact them in all ways possible. If the group you pick does not respond ever, mark them as inactive and move on to someone else.
 
Also, if they get back to you or if you manage to get put into the facebook/mailing list, you need to like, actually go to the meetings. Even if it’s really annoying and you’re tired and smelly and you just want to get home and watch cartoons. If you cannot make it because it’s in BFE at 6PM when you get out of work at 5:30PM, ask them if it’s okay to arrive late or if they ever have meetings in alternate locations. If location and time-wise it does not work out for you, find another group that either meets your needs or is willing to accommodate you.
 
## Environmental Awareness
 
“There is no ethic-“ shhh…. There we go.
 
We get it, we were born with original sin under capitalism and we shall die attempting to absolve ourselves of this evil, so why bother at all until Commie Savior descends from the skies and leads us into glorious Upturning? Bla bla you can fit all the polluters in the world into two greyhound buses blab la 100 companies.
 
Fam. We get it. Nobody is saying that if you use soalr power, you will single-handedly stop climate change. We know we need structural change. (See: the sections on organizing and liberal democracy.) BUT! The bad news is that an environmentally friendly world will require us to change our habits, no matter what. You could burn all the CEOs of the polluting companies alive, and it might stop emissions but we would eventually have to change to solar power, eat significantly less meat/dairy, buy less water intensive textiles, etc.
 
So much like voting is not enough but you should still do it, individual or small collective actions are not enough but you should still do it – IF YOU CAN. I’m not saying that if you’re living paycheck to paycheck you should buy organic vegetables from your local farmer. IF YOU HAVE THE MEANS (and be honest with yourself – you know whether you have the means or not), do these things. If you can eat chipotle once a week, you can probably do some of these things.
 
[Blab la list of baby environmental actions]
 
Zero Waste
 
Composting
 
Buying organic/fair trade items – why? Well if workers are paid a fair wage, the price of the item they make will inevitably go up because right now they’re being outrageously underpaid. A cotton t shirt assembled in the US with all the worker protections that would require and the fair wage would end up costing $50. And that’s not even including profit. Macklemore, eat your heart out – a t shirt that’s less than $50 is probably made using slave labor. And unfortunately, even the most ethical fair trade organic blab la will have some slave labor because supply chains, even for companies that wanna be ~super ethical~, are notoriously opaque and hard to audit. Nevertheless, if you have a choice between “lots of slavery” and “less slavery,” I think “less slavery” is objectively better. If you find an option that is “no slavery,” plase let me know.
 
Other than thrifting, which is just delayed slavery. Someone had to buy that thing for you to thrift it, friend. And if everyone thrifted in the world and nobody ever bought new, eventually we would run out of clothes… and then we would have to re learn how to produce textiles… and then we’d be like “fuck how do we do this ethically again? We never figured it out because everyone decided to use the non-renewable resource of ‘used clothes’.” We need to figure out a way to make sure everyone involved in the creation of clothes gets paid what they deserve, has safety, and ideally owns the means of production. We don’t do that by delaying our participation in capitalism for our own moral benefit.
 
HOWEVER reducing consumption in general is good, so I’m not saying thrifting is evil. Just that it’s not actually the silver bullet some folks make it out to be. There ARE too many clothes in the world right now so reusing our old clothes is critical to avoiding massive waste. Radical reusing!
 
Starting a community farm. No, capitalists are not going to take away your community farm. If droit-wing nutjobs can have their cult farms in the middle of nowhere as long as there’ sno child abuse (ahem, WACO), why can’t we? They’re not all-seeing, I promise you. Stop letting your paranoia stop you from doing community stuff. You can totally have a community garden or have everyone in an apartment grow stuff together and then have a big community meal. Or let the introverts go back to their rooms – some of us need a break from togetherness! Anyway if you can do this please do.
 
Recycling – start a recycling program! At your school! Your office building! It’s possible! Google it!
 
Solar stuff – yeah!
 
Eating/using fewer animal products – vegetarians and vegans have tons of guides for this. Do what you can.
 
Much like “social issues regarding marginalized peoples,” environmental friendliness is something that should be on everyone’s radar. It’s not a special topic like “housing” where you can be like, this is my thing. Whether you are working on housing, immigration, whatever, you must have the environment on your mind just as you much have issues regarding marginalization on your mind. It is an important part of intersectionalism.
 
## Marginalized Groups
 
Fun fact – Cuba, a “socialist” country, is run by old white men who throw you in jail if you talk shit about them! Damn, if I wanted that I could just stay in the US, huh? There’s no point in r E v O l U t I o N if the new boss is the same as the old boss. That means that people from historically and currently marginalized groups must be consulted at every step of the decision making progress and must be taken into account for everything. Much like the environment, this is a case where you can afford to just do it occasionally. Otherwise we’re gonna create a world that’s real nice for cis straight white able men (insert other privileges here if you’d like I don’t care) while everyone else gets the same deal as before, only now everything is red. Uncool.
 
There’s a ton of reading about this on tha innanetz so I’m just gonna link that here. You are probably going to ghave to do a lot of reading if you’ve never really cared about this before, because you need to build up a really strong base on a lot of issues.
 
Note: sometimes people will say stuff like “if you don’t have any friends who are [x marginalized group],are you really an ally?” And let me tell you, this sounds nice in theory but it’s extremely creepy and othering to try to become someone’s friend for the sole purpose of ticking off a checkbox on your mental list of marginalized demographics. Do not try to befriend people so you can meet your quota of wokeness. If you somehow have no friends, not even acquiantances, of some broad group, maybe consider that you may be doing something to drive them away, and adjust your behavior appropriately. But don’t chase after a member of that group so you can Collect ‘Em All. It’s better to be colleagues than weirdo fake friends, okay?

## Internet-Friendly Mutual Aid
 
Creating open-access audio books (librevox) for blind/other people who prefer spoken material to written
 
Captioning videos and creating transcripts for podcasts for people who are deaf/hard of hearing or have other disabilities that may affect their hearing.
 
Translating things to other languages

## The stuff in rebel steps